package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginModal {

    private final WebDriver driver;

    private By tbEmail = By.xpath("//input[@id='email']");
    private By tbPassword = By.xpath("//input[@id='password']");

    public LoginModal(WebDriver driver){
        this.driver = driver;
    }

    public void login(String email, String password){
        driver.findElement(tbEmail).sendKeys(email);
        driver.findElement(tbPassword).sendKeys(password);

        WebElement formBtn = driver.findElement(By.xpath("//button[@tabindex='3']"));
        formBtn.click();

    }
}

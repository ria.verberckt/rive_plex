package be.ordina;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnSignUp = By.xpath("//a[@class='signup button']");
    private By btnSignIn = By.xpath("//a[@class='signin']");


    public HomePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickSignUp(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnSignUp));
        driver.findElement(btnSignUp).click();

        driver.switchTo().frame("fedauth-iFrame");
    }

    public void clickSignIn(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnSignIn));
        driver.findElement(btnSignIn).click();

        driver.switchTo().frame("fedauth-iFrame");
    }

    /*
    public String getSuccessMessage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(lblLoginInformation));
        String message = driver.findElement(lblLoginInformation).getText();
        return message;
    }*/
}
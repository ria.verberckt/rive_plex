package be.ordina;

import static org.junit.Assert.assertTrue;

import be.ordina.pages.StartPage;
import be.ordina.pages.LoginModal;
import be.ordina.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.security.AccessControlContext;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private WebDriver driver;
    private WebDriverWait wait;

    private By confirmationTextHome = By.xpath("//span[contains(@class,'PageHeaderBreadcrumbButton')]");
    private By posterCardLink = By.xpath("//a[contains(@class,'PosterCardLink-link')]");
    private By btnLaunch = By.xpath("//a[@class='launch button']");
    private By btnPlay = By.xpath("//*[@id='content']/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div/div[1]/div/button[1]");
    private By iconPause = By.id("plex-icon-player-pause-560");
    private By controlsContainer = By.xpath("//div[contains(@class,'ControlsContainer')]");
    private By btnAgree = By.xpath("//button[contains(@data-uid,'id-263')]");
    //private By  = By.xpath("//");

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Users/RiVe/Documents/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.plex.tv");
        driver.manage().window().maximize();
        this.wait = new WebDriverWait(driver, 10);}

    @After
    public void tearDown(){
        //driver.close();
    }

    public void logInAndLaunch() throws IOException, InterruptedException {
        StartPage startPage = new StartPage(driver);
        LoginModal loginModal = new LoginModal(driver);
        startPage.clickSignIn();

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

        loginModal.signIn("PlexEmailRia@mailinator.com", "testPaswoord1");

        SeleniumUtils.screenshot(driver, "screenshot");

        driver.switchTo().parentFrame();

        wait.until(ExpectedConditions.visibilityOfElementLocated(btnLaunch));
        driver.findElement(btnLaunch).click();
    }

    public void signUpAndLaunch() throws IOException, InterruptedException {
        StartPage startPage = new StartPage(driver);
        LoginModal loginModal = new LoginModal(driver);
        startPage.clickSignUp();

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

        String inputEmail = getAlphanumericString(8) + "@mailinator.com";
        loginModal.signUp(inputEmail, "testPaswoord1");

        SeleniumUtils.screenshot(driver, "screenshot");

        driver.switchTo().parentFrame();
    }

    @Test
    public void successfullSignUp() throws IOException, InterruptedException {
        signUpAndLaunch();

        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmationTextHome));
        String messageHome = driver.findElement(confirmationTextHome).getText();
        assertTrue(messageHome.contains("Home"));
    }

    @Test
    public void successfullLogIn() throws IOException, InterruptedException {
        logInAndLaunch();

        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmationTextHome));
        String messageHome = driver.findElement(confirmationTextHome).getText();
        assertTrue(messageHome.contains("Home"));
    }

    @Test
    public void successfullStartStreaming() throws InterruptedException, IOException {
        signUpAndLaunch();

        wait.until(ExpectedConditions.visibilityOfElementLocated(posterCardLink));
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(posterCardLink)).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnPlay));
        driver.findElement(btnPlay).click();

        //wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("downloadFileFrame"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnAgree));
        driver.findElement(btnAgree).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(controlsContainer));
        action.moveToElement(driver.findElement(controlsContainer)).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(iconPause));
        String ariaHidden = driver.findElement(iconPause).getText();
        assertTrue(ariaHidden.equals("true"));

    }



    static String getAlphanumericString(int n){
        // chose a character random from this string
        String AlphaNumericString = "ABCEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789" + "abcdefghijklmnopqrstuvwxyz";

        // create StringBuffer size of AlphaNumeriString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i<n; i++){
            // generate a random number between 0
            // to AlphaNumericString variabele length
            int index = (int) (AlphaNumericString.length()
                    * Math.random() );

            // add character one by one in end of sb
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

}
